# FIR Jerk-Constrained Trajectory Generation

FIR filter based algorithm which generates smooth jerk-constrained trajectories. Inputs are initial state, ending set-point, and kinematic constraints, and a jerk limit.